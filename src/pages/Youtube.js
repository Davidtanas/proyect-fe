import React from "react";
import YoutubeLink from "../Components/Footer/youtube";

function Youtube() {
  return (
    <>
      <YoutubeLink />
    </>
  );
}

export default Youtube;
