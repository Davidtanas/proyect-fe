import React from "react";

import TwitterLink from "../Components/Footer/twitter";

function Twitter() {
  return (
    <>
      <TwitterLink />
    </>
  );
}

export default Twitter;
