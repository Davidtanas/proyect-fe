import React, { useState } from "react";
import AppMenu from "../AppMenu";
import { Button2, SubmitButton } from "../ButtonElements";

import {
  ContainerPrincipal,
  ProfileBgPrincipal,
  ProfileColumnPrincipal,
  ProfileContentPrincipal,
  ProfileH1Principal,
  ProfilePPrincipal,
  AaaPrincipal,
  ProfileBtnWrapperPrincipal,
  CCField,
  CCLabel,
  CCForm,
  CCArea,
  ImageUpload,
  ImageArea,
  Footer,
  ImgImg,
  HomeFa,
} from "./epElements.js";

const EditarPerfil = () => {
  const [hover, setHover] = useState(false);
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");
  const [oldPassword, setOldPassword] = useState("");
  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");
  const onHover = () => {
    setHover(!hover);
  };

  const loadFile = function (event) {
    const image = document.getElementById("output");
    image.src = URL.createObjectURL(event.target.files[0]);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setError(null);

    if (password !== password2) {
      setError("Las contraseñas no coinciden");
      return;
    }
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API}/usuarios/password`,
        {
          method: "PUT",
          headers: {
            "Content-Type": "application/json",
            Authorization: localStorage.getItem("token"),
          },
          body: JSON.stringify({ oldPassword, newPassword: password }),
        }
      );

      const json = await response.json();

      if (!response.ok) {
        throw new Error(json.message);
      }

      setSuccess("Contraseña actualizada correctamente");
    } catch (e) {
      setError(e.message);
    }
  };

  return (
    <>
      <ContainerPrincipal id="home">
        <ProfileBgPrincipal></ProfileBgPrincipal>
        <ProfileContentPrincipal>
          <ProfileH1Principal>Bienvenido,</ProfileH1Principal>
          <ProfileColumnPrincipal>
            <AaaPrincipal>
              {/*               <ImageArea>
                <ImageUpload
                  type="file"
                  accept="image/*"
                  name="image"
                  onChange={loadFile}
                />
                <ImgImg id="output" />
              </ImageArea> */}

              <CCForm>
                <ProfilePPrincipal> Change your password </ProfilePPrincipal>
                <CCLabel htmlFor="password"> Old Password </CCLabel>
                <CCField
                  type="password"
                  id="old-password"
                  value={oldPassword}
                  onChange={(e) => setOldPassword(e.target.value)}
                  required
                />{" "}
                <CCLabel htmlFor="password"> Password </CCLabel>
                <CCField
                  type="password"
                  id="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  required
                />
                <CCLabel htmlFor="password2"> Verify Password </CCLabel>
                <CCField
                  type="password"
                  id="password2"
                  value={password2}
                  onChange={(e) => setPassword2(e.target.value)}
                  required
                />
              </CCForm>
              <ProfileBtnWrapperPrincipal>
                <SubmitButton
                  smooth={true}
                  duration={500}
                  spy={true}
                  exact="true"
                  offset={-80}
                  primary="true"
                  dark="true"
                  onMouseEnter={onHover}
                  onMouseLeave={onHover}
                  onClick={handleSubmit}
                >
                  {" "}
                  Cambiar{" "}
                </SubmitButton>
              </ProfileBtnWrapperPrincipal>
              {error ? <p style={{ color: "red" }}>{error}</p> : null}
              {success ? <p style={{ color: "green" }}>{success}</p> : null}
            </AaaPrincipal>
            {/*    <AaaPrincipal>
              <ProfilePPrincipal> Busca un espacio </ProfilePPrincipal>
              <CCLabel htmlFor="username"> Username </CCLabel>
              <CCField type="text" id="Username" />
              <CCLabel htmlFor="email"> Email </CCLabel>
              <CCField type="email" id="eemail" />
              <CCLabel htmlFor="phone"> Phone Number </CCLabel>
              <CCField type="number" id="phone" />
              <CCLabel htmlFor="aboutme"> About me </CCLabel>
              <CCArea></CCArea>
              <ProfileBtnWrapperPrincipal>
                <Button2
                  to="/spotlight"
                  smooth={true}
                  duration={500}
                  spy={true}
                  exact="true"
                  offset={-80}
                  primary="true"
                  dark="true"
                  onMouseEnter={onHover}
                  onMouseLeave={onHover}
                >
                  {" "}
                  Guardar Cambios{" "}
                </Button2>
              </ProfileBtnWrapperPrincipal>
            </AaaPrincipal> */}
          </ProfileColumnPrincipal>
          <AppMenu />
        </ProfileContentPrincipal>
      </ContainerPrincipal>
    </>
  );
};

export default EditarPerfil;
