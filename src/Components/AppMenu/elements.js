import { FaHome } from "react-icons/fa";
import styled from "styled-components";

export const Footer = styled.div`
  display: flex;
  justify-content: center;
  margin: 60px;
  height: 40px;
  width: 90vw;
  text-align: center;
  & button {
    margin: 0 15px;
  }
`;

export const HomeFa = styled(FaHome)`
  text-align: center;
  margin-left: 8px;
  font-size: 20px;
`;
