import React, { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { Button2, SubmitButton } from "../ButtonElements";
import { Footer, HomeFa } from "./elements";

const AppMenu = () => {
  const navigate = useNavigate();

  const handleLogout = () => {
    localStorage.removeItem("token");
    navigate("/");
  };

  useEffect(() => {
    if (!localStorage.getItem("token")) navigate("/");
  }, [navigate]);
  return (
    <Footer>
      <Button2
        to="/"
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        primary="true"
        dark="true"
      >
        <HomeFa></HomeFa>
      </Button2>{" "}
      <SubmitButton
        onClick={handleLogout}
        smooth={true}
        duration={500}
        spy={true}
        exact="true"
        offset={-80}
        primary="true"
        dark="true"
      >
        Logout
      </SubmitButton>
    </Footer>
  );
};

export default AppMenu;
