import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import {
  Container,
  FormLabel,
  FormInput,
  Text,
  TextLink,
  Form,
  FormSubmit,
  FormContent,
  FormH1,
  FormWrap,
  Icon,
} from "./SigninElements";

const SignInnn = () => {
  const navigate = useNavigate();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error, setError] = useState("");
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API}/usuarios/login`,
        {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify({ email, password }),
        }
      );

      const json = await response.json();

      localStorage.token = json.token;
      if (!response.ok) {
        throw new Error(json.message);
      }

      navigate("/australia");
    } catch (e) {
      setError(e.message);
    }
  };

  return (
    <>
      <Container>
        <FormWrap>
          <Icon to="/"> ANDA </Icon>
          <FormContent>
            <Form action="#">
              <FormH1> Sign into your account </FormH1>
              <FormLabel htmlFor="email"> Email </FormLabel>
              <FormInput
                type="email"
                id="email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                required
              />
              <FormLabel htmlFor="password"> Password </FormLabel>
              <FormInput
                type="password"
                id="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                required
              />
              <FormSubmit onClick={handleSubmit}> Continue </FormSubmit>
              <TextLink>
                <Text to="/SignUp">
                  {" "}
                  Don't have an account? Sign Up for free!{" "}
                </Text>
              </TextLink>
            </Form>
            {error ? <p style={{ color: "red" }}>{error}</p> : null}
          </FormContent>
        </FormWrap>
      </Container>
    </>
  );
};

export default SignInnn;
