import React, { useState } from "react";
import {
  DataContainer,
  DataObj,
  DataH1,
  IntroBox,
  DescriptionBox,
  IntroLeftSide,
  IntroRightSide,
  PreviewImg,
  ImgContainer,
  DataInformation,
  DataDefaultText,
  DataDefaultTextStatus,
  DataValueText,
  UselessReviewArea,
  Description,
  Reservar,
} from "./resuElements";

import StarRating from "./review";
import ReservaStatus from "./status";

const Resultados = ({
  descripcion,
  precio,
  capacidad,
  direccion,
  razonsocial,
  contacto,
  id,
}) => {
  const [reservacion, setReservacion] = useState(false);

  const reservaboton = () => {
    if (Reservar.onClick === true) {
      setReservacion(true);
      console.log("clicked");
    } else {
      setReservacion(false);
    }
  };
  return (
    <>
      <DataContainer>
        <DataObj>
          <IntroBox>
            <IntroLeftSide>
              <ImgContainer>
                <PreviewImg></PreviewImg>
              </ImgContainer>
            </IntroLeftSide>
            <IntroRightSide>
              <DataH1> Espacio {id} </DataH1>
              <DataInformation>
                <DataDefaultText>
                  {" "}
                  Name: <DataValueText> {razonsocial} </DataValueText>{" "}
                </DataDefaultText>
                <DataDefaultText>
                  {" "}
                  Location: <DataValueText> {direccion} </DataValueText>{" "}
                </DataDefaultText>
                <DataDefaultText>
                  {" "}
                  Price: <DataValueText> ${precio}/noche </DataValueText>{" "}
                </DataDefaultText>
                <DataDefaultTextStatus>
                  Capacidad: <DataValueText> {capacidad} </DataValueText>{" "}
                </DataDefaultTextStatus>{" "}
                <DataDefaultTextStatus>
                  Contacto: <DataValueText> {contacto} </DataValueText>{" "}
                </DataDefaultTextStatus>
              </DataInformation>
            </IntroRightSide>
          </IntroBox>
          <DescriptionBox>
            <UselessReviewArea>
              <StarRating> </StarRating>
            </UselessReviewArea>
            <Description>{descripcion}</Description>
            {/* <Reservar onClick={reservacion}> Reservar </Reservar> */}
          </DescriptionBox>
        </DataObj>
      </DataContainer>
    </>
  );
};

export default Resultados;
