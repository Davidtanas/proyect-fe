import React, { useState } from "react";
import AppMenu from "../AppMenu";
import { SubmitButton } from "../ButtonElements";
import Resultados from "../Resultados";
import {
  ContainerGrande,
  FormLabel,
  FormInput,
  Form,
  FormButton,
  FormContent,
  FormH1,
  FormWrap,
  FormLabelPapa,
  Icon,
  ResultsContainer,
  BackContainer,
} from "./BuscaElements";

const A123 = () => {
  const [location, setLocation] = useState("");
  const [activity, setActivity] = useState("");
  const [searchResults, setSearchResults] = useState();
  const [error, setError] = useState("");

  const backToSearch = () => {
    setSearchResults(undefined);
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    setError(null);
    try {
      const response = await fetch(
        `${process.env.REACT_APP_API}/espacios?activity=${activity}&location=${location}`,
        {
          method: "GET",
          headers: {
            "Content-Type": "application/json",
            Authorization: localStorage.getItem("token"),
          },
        }
      );

      const espacios = await response.json();

      if (!response.ok) {
        throw new Error(espacios.message);
      }
      setSearchResults(espacios.espacios);
    } catch (e) {
      setError(e.message);
    }
  };

  return (
    <>
      <ContainerGrande>
        <FormWrap>
          <Icon to="/"> ANDA </Icon>
          {searchResults ? (
            <>
              <ResultsContainer>
                {searchResults.length > 0 ? (
                  searchResults.map((result) => <Resultados {...result} />)
                ) : (
                  <p>No results for that search</p>
                )}
              </ResultsContainer>
              <BackContainer>
                <SubmitButton primary dark onClick={backToSearch}>
                  {" "}
                  Back to search{" "}
                </SubmitButton>
              </BackContainer>
            </>
          ) : (
            <FormContent>
              <Form action="#">
                <FormH1> Find your ideal place by </FormH1>
                <FormLabel htmlFor="password"> Activity</FormLabel>
                <FormInput
                  type="activity"
                  id="activity"
                  value={activity}
                  onChange={(e) => setActivity(e.target.value)}
                  required
                />{" "}
                <FormLabel htmlFor="location"> Location </FormLabel>
                <FormInput
                  type="location"
                  id="location"
                  value={location}
                  onChange={(e) => setLocation(e.target.value)}
                  required
                />
                {/*               <FormLabelPapa>
                <FormLabel htmlFor="for"> Duration </FormLabel>
                <FormInput type="date" />
                <FormLabel htmlFor="for"> </FormLabel>
                <FormInput type="date" />
              </FormLabelPapa> */}
                <SubmitButton primary dark onClick={handleSubmit}>
                  {" "}
                  Search{" "}
                </SubmitButton>
              </Form>
              {error ? <p style={{ color: "red" }}>{error}</p> : null}
            </FormContent>
          )}
        </FormWrap>
      </ContainerGrande>
      <AppMenu />
    </>
  );
};

export default A123;
