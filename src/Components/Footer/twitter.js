import React, { useEffect } from "react";

function TwitterLink() {
  useEffect(() => {
    window.location.href =
      "https://twitter.com/hackaboss_?ref_src=twsrc%5Egoogle%7Ctwcamp%5Eserp%7Ctwgr%5Eauthor";
  }, []);

  return <></>;
}

export default TwitterLink;
