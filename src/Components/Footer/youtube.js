import React, { useEffect } from "react";

function YoutubeLink() {
  useEffect(() => {
    window.location.href =
      "https://www.youtube.com/channel/UC7EEBdXePw3t-_ttj0V0FDA";
  }, []);

  return <></>;
}

export default YoutubeLink;
